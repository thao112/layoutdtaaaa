package com.example.lam.layoutdtaaaa;

//import android.app.Fragment;
//import android.support.annotation.NonNull;
//import android.support.design.widget.BottomNavigationView;


//import android.support.v4.app.FragmentTransaction;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
//import android.view.MenuItem;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

//import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.lam.layoutdtaaaa.Fragment.Fragment_A;
import com.example.lam.layoutdtaaaa.Fragment.Fragment_B;
import com.example.lam.layoutdtaaaa.Fragment.Fragment_C;
import com.example.lam.layoutdtaaaa.Fragment.Fragment_D;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    AdapterIMG mRcvAdapter;
    List<String> data;

    BottomNavigationView mMainNav;
    FrameLayout mMainFrame;

    private Fragment_A fragment_a;
    private Fragment_B fragment_b;
    private Fragment_C fragment_c;
    private Fragment_D fragment_d;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//        data = new ArrayList<>();
//        data.add("Nguyễn Minh Hưng");
//        data.add("Hoàng Minh Lợi");
//        data.add("Nguyễn Duy Bảo");
//        data.add("Nguyễn Ngọc Doanh");
//        data.add("Nguyễn Phạm Thế Hà");
//        data.add("Trần Anh Đức");
//        data.add("Trần Minh Hải");
//        mRcvAdapter = new AdapterIMG(data);
//
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//
//        mRecyclerView.setLayoutManager(layoutManager);
//        mRecyclerView.setAdapter(mRcvAdapter);

        fragment_a = new Fragment_A();
        fragment_b = new Fragment_B();
        fragment_c = new Fragment_C();
        fragment_d = new Fragment_D();

        mMainFrame = (FrameLayout) findViewById(R.id.frame_layout);
        mMainNav = (BottomNavigationView) findViewById(R.id.navigation);

//        fragmentManager = getFragmentManager();
//        fragmentTransaction = fragmentManager.beginTransaction();
//        fragment = null;
//        setFragment(fragment_a);

        mMainNav.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {

                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragment = null;

                switch (item.getItemId()){
                    case R.id.action_item1:
                        setFragment(fragment_a);
//                        mMainNav.setItemBackgroundResource(R.color.colorPrimary);
                        item.setIcon(R.drawable.ic_action_homed);
//                        mMainNav.set;
//                        fragment = new Fragment_A();
//                        AddFragmentA();
                        return;
                    case R.id.action_item2:
                        setFragment(fragment_b);
//                        fragment = new Fragment_B();
//                        AddFragmentA();
                        return;
                    case R.id.action_item3:
                        setFragment(fragment_c);
//                        fragment = new Fragment_C();
//                        AddFragmentA();
                        return;
                    case R.id.action_item4:
                        setFragment(fragment_d);
//                        fragment = new Fragment_D();
//                        AddFragmentA();
                        return;
                    default:
                        return;
                }

            }
        });

    }

    public void AddFragmentA(){
        fragmentTransaction.add(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
    }

    public void HideBottomViewOnScrollBehaviors(Context context, AttributeSet attrs){

    }

    private void setFragment(Fragment fragment){
//        fragmentManager = getFragmentManager();
//        fragmentTransaction = fragmentManager.beginTransaction();
//        fragment = null;
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
    }
}
