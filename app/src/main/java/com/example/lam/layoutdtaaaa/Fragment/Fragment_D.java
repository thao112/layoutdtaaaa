package com.example.lam.layoutdtaaaa.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lam.layoutdtaaaa.R;

/**
 * Created by lam on 7/8/2018.
 */

public class Fragment_D extends Fragment {

        public static Fragment_D newInstance(){
        Fragment_D fragment_d = new Fragment_D();
        return fragment_d;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_d, container,false);
        return view;
    }
}
