package com.example.lam.layoutdtaaaa.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lam.layoutdtaaaa.AdapterIMG;
import com.example.lam.layoutdtaaaa.R;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.SearchParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lam on 7/8/2018.
 */

public class Fragment_A extends Fragment{
    RecyclerView mRecyclerView;
    AdapterIMG mRcvAdapter;
    List<String> data;

//    public static Fragment_A newInstance(){
//        Fragment_A fragment_a = new Fragment_A();
//        return fragment_a;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_a, container,false);

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String apiKey = "dda3931f7c544bb6eab964644af778db";
                    String sharedSecret = "34db07a4b09deec6";
                    REST rest = new REST();
                    Flickr flickrClient = new Flickr(apiKey, sharedSecret, rest);

                    SearchParameters searchParameters = new SearchParameters();
                    searchParameters.setLatitude("2000");
                    searchParameters.setLongitude("2000");
                    searchParameters.setRadius(3); // Km around the given location where to search pictures

                    PhotoList photos = flickrClient.getPhotosInterface().search(searchParameters,5,1);

                } catch (Exception ex) {
//                    Log.d(Fragment_A.LOG_TAG, ex.getLocalizedMessage());
                }
            }
        });

        thread.start();


        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        // 2. set layoutManger
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // this is data fro recycler view
        data= new ArrayList<>(); {
            data.add("Nguyễn Minh Hưng");
            data.add("Hoàng Minh Lợi");
            data.add("Nguyễn Duy Bảo");
            data.add("Nguyễn Ngọc Doanh");
            data.add("Nguyễn Phạm Thế Hà");
            data.add("Trần Anh Đức");
            data.add("Trần Minh Hải");
        }


        // 3. create an adapter
        mRcvAdapter = new AdapterIMG(data);
//        // 4. set adapter
        mRecyclerView.setAdapter(mRcvAdapter);
//        // 5. set item animator to DefaultAnimator
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }
}
