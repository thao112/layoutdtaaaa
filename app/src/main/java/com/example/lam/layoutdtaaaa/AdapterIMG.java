package com.example.lam.layoutdtaaaa;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lam on 7/7/2018.
 */

public class AdapterIMG extends RecyclerView.Adapter<AdapterIMG.AdapterHolder>{

    private List<String> data = new ArrayList<>();

    public AdapterIMG(List<String> data) {
        this.data = data;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_img, parent, false);
        return new AdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterHolder holder, int position) {
        holder.txtUserName.setText(data.get(position));
        holder.imvB.setImageResource(R.drawable.ic_action_home);
        holder.imvM1.setImageResource(R.drawable.ic_action_home);
        holder.imvM2.setImageResource(R.drawable.ic_action_home);
        holder.imgM3.setImageResource(R.drawable.ic_action_home);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class AdapterHolder extends RecyclerView.ViewHolder {
        TextView txtUserName;
        ImageView imvB, imvM1, imvM2, imgM3;
        public AdapterHolder(View itemView) {
            super(itemView);
            txtUserName = (TextView) itemView.findViewById(R.id.textV);
            imvB = (ImageView) itemView.findViewById(R.id.imgBig);
            imvM1 = (ImageView) itemView.findViewById(R.id.img1);
            imvM2 = (ImageView) itemView.findViewById(R.id.img2);
            imgM3 = (ImageView) itemView.findViewById(R.id.img3);
        }
    }

}
